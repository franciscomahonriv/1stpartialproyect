/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructure.st1525;

import mx.edu.utr.datastructures.List;

/**
 *
 * @author frederic
 */
public class ArrayList implements List {

    int[] arrayList = new int[10];
    int size;
    Object[] elements;

    @Override
    public boolean add(Object element) {
        ensureCapacity(size + 1);
        elements[size++] = element;
        return true;

    }

    @Override
    public void add(int index, Object element) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        for (int i = 0; size < 10; i++) {
            elements[i] = null;
            size = 0;
        }
    }

    @Override
    public Object get(int index) {
        for (int i = 0; i <= size; i++) {
            if (index == i - 1) {
                elements[i] = index;
            }
        }
        return elements[index];

    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(elements[i])) {
                    return i;

                }
            }
        }
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Object remove(int index) {
          outOfBound(index);
          object oldElement=(object) elements[index];
          int  numberMoved =size-index-1;
          if (numberMoved>0){
              System.arraycopy(elements,index+1,elements,index,numberMoved);}
    elements[--size]=null;
    return oldElement;
    }

    @Override
    public Object set(int index, Object element) {

        Object old = elements[index];
        elements[index] = elements;
        return old;
    }

    @Override
    public int size() {

        return size;
    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        int newCapacity;
        if (minCapacity > oldCapacity) {
            newCapacity = oldCapacity * 2;
            arrayList = new int[newCapacity];
            for (int i = 0; i >= oldCapacity; i++) {
                arrayList[i] = newCapacity;
            }
        }
    }

    private void outOfBound(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
